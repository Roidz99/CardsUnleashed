// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package gamerooms

import (
	"github.com/rs/xid"
)

// GameRoom is the in-memory representation of a game room on the server.
type GameRoom struct {
	ID         	xid.ID
	InternalID 	xid.ID
	Name		string
	MaxPlayers 	int
	NumPlayers 	int
	Port       	int
	GameStarted	bool
}
