// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"dsk/config"
	"dsk/gamerooms"
	"dsk/handlers"

	"github.com/rs/xid"
)

func main() {
	// This logger is used for writing information messages.
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)

	// This logger is used for writing error messages.
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	// Open the server's configuration file.
	file, err := os.Open("conf.json")
	if err != nil {
		errorLog.Fatal(err)
	}

	// Decode the server's configuration file into a Configuration object.
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := config.Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		errorLog.Fatal(err)
	}

	// Open the database based on the configuration connection string.
	db, err := config.NewDB(configuration.DBConnectionString)
	if err != nil {
		errorLog.Fatal(err)
	}

	// Set up a new environment for the server. The environment contains different
	// objects that are useful across the entire application.
	rooms := make(map[xid.ID]*gamerooms.GameRoom)
	env := &config.Env{
		Config:      configuration,
		DB:          db,
		GameRooms:   rooms,
		CurrentPort: configuration.GameServerPort,
		InfoLog:     infoLog,
		ErrorLog:    errorLog}

	// Register the HTTP handlers of the server.
	mux := http.NewServeMux()
	mux.Handle("/register", handlers.RegisterHandler(env))
	mux.Handle("/login", handlers.LoginHandler(env))
	mux.Handle("/create_game_room", handlers.CreateGameRoomHandler(env))
	mux.Handle("/find_game_rooms", handlers.FindGameRoomsHandler(env))
	mux.Handle("/join_game_room", handlers.JoinGameRoomHandler(env))
	mux.Handle("/update_game_room", handlers.UpdateGameRoomHandler(env))

	// Start the server.
	infoLog.Printf("############################")
	infoLog.Printf("#   Dedicated Server Kit   #")
	infoLog.Printf("############################")
	infoLog.Printf("")
	infoLog.Printf(fmt.Sprintf("Starting server at %v:%v", configuration.IPAddress, configuration.Port))
	errorLog.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", configuration.Port), mux))
}
