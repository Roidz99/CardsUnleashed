// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"net/http"
	"strings"

	"dsk/config"

	"github.com/dgrijalva/jwt-go"
)

// Claims is our custom claims type, containing the username.
type Claims struct {
	Username string
	jwt.StandardClaims
}

// IsAuthorizedRequest is an utility function to check if the given request is authorized.
func IsAuthorizedRequest(w http.ResponseWriter, r *http.Request, env *config.Env) bool {
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer")
	if len(splitToken) != 2 {
		SetErrorResponse(w, "Bad request.", "This request is not valid.", 400)
		return false
	}

	tokenString := strings.TrimSpace(splitToken[1])

	claims := &Claims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(env.Config.JwtKey), nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			SetErrorResponse(w, "Unauthorized.", "You do not have access to this route.", 403)
			return false
		}
		SetErrorResponse(w, "Bad request.", "This request is not valid.", 400)
		return false
	}

	if !token.Valid {
		SetErrorResponse(w, "Unauthorized.", "You do not have access to this route.", 403)
		return false
	}

	return true
}
