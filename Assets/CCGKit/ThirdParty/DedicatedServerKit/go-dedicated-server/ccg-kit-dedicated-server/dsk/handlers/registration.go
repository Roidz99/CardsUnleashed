// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"net/http"
	"strings"

	"dsk/auth"
	"dsk/config"
	"dsk/models"
)

type registrationRequest struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func (request *registrationRequest) Validate() bool {
	if strings.TrimSpace(request.Email) == "" {
		return false
	}

	if strings.TrimSpace(request.Username) == "" {
		return false
	}

	if strings.TrimSpace(request.Password) == "" {
		return false
	}

	return true
}

// RegisterHandler is the handler of the "register" route.
func RegisterHandler(env *config.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Make sure the request is a POST.
		if r.Method != http.MethodPost {
			SetErrorResponse(w, "Invalid method.", "This endpoint expects a POST method.", 400)
			return
		}

		// Parse the form parameters.
		r.ParseForm()
		email := r.FormValue("email")
		username := r.FormValue("username")
		password := r.FormValue("password")
		request := &registrationRequest{
			Email:    email,
			Username: username,
			Password: password,
		}

		// Validate the form parameters.
		if !request.Validate() {
			SetErrorResponse(w, "Missing parameters.", "This endpoint expects an email (string), username (string) and password (string).", 400)
			return
		}

		userExists := models.UserWithNameExists(env.DB, username)
		if userExists {
			SetErrorResponse(w, "Existing username.", "The passed username already exists.", 400)
			return
		}

		userExists = models.UserWithEmailExists(env.DB, email)
		if userExists {
			SetErrorResponse(w, "Existing email.", "The passed email already exists.", 400)
			return
		}

		hashedPassword, err := auth.GenerateHashedPassword(password)
		if err != nil {
			SetErrorResponse(w, "Internal server error.", "A valid password hash could not be generated.", 500)
		}

		if !userExists {
			models.AddUser(env.DB, username, email, hashedPassword)
		} else {
			SetErrorResponse(w, "Internal server error.", "There was a problem with the database.", 500)
			return
		}

		w.WriteHeader(200)
	})
}
