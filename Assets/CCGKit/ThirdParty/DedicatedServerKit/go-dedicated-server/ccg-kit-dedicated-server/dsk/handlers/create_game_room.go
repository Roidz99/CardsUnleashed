// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"encoding/json"
	"net/http"
	"os/exec"
	"strconv"

	"dsk/config"
	"dsk/gamerooms"

	"github.com/rs/xid"
)

type createGameRoomRequest struct {
	MaxPlayers int
}

type createGameRoomResponse struct {
	IPAddress string `json:"ip_address"`
	Port      int    `json:"port"`
}

// CreateGameRoomHandler is the handler of the "create_game_room" route.
func CreateGameRoomHandler(env *config.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !IsAuthorizedRequest(w, r, env) {
			return
		}

		// Make sure the request is a POST.
		if r.Method != http.MethodPost {
			SetErrorResponse(w, "Invalid method.", "This endpoint expects a POST method.", 400)
			return
		}

		// Parse the form parameters.
		r.ParseForm()
		maxPlayers, err := strconv.ParseInt(r.FormValue("max_players"), 10, 0)
		if err != nil {
			SetErrorResponse(w, "Wrong parameter.", "This endpoint expects a max_players (int).", 400)
			return
		}

		request := &createGameRoomRequest{
			MaxPlayers: int(maxPlayers),
		}

		id := xid.New()
		internalID := xid.New()
		name := r.FormValue("name")
		var room = &gamerooms.GameRoom{
			ID:         id,
			InternalID: internalID,
			Name: 	    name,
			MaxPlayers: request.MaxPlayers,
			NumPlayers: 0,
			Port:       env.CurrentPort,
		}
		env.GameRooms[id] = room

		port := env.CurrentPort
		env.CurrentPort++

		portStr := strconv.FormatInt(int64(port), 10)
		args := []string{portStr, id.String(), internalID.String()}
		cmd := exec.Command(env.Config.GameServerBinPath, args...)
		err = cmd.Start()
		if err != nil {
			SetErrorResponse(w, "Internal server error.", "A game server could not be started.", 500)
			return
		}

		go func() {
			err = cmd.Wait()
			delete(env.GameRooms, id)
		}()

		response := &createGameRoomResponse{
			IPAddress: env.Config.IPAddress,
			Port:      port,
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(response)
	})
}
