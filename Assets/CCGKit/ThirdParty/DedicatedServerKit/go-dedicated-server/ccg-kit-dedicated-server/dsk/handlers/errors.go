// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"encoding/json"
	"net/http"
)

// ErrorResponse represents a generic error response from a HTTP handler. Based on the RFC-7807 standard.
type ErrorResponse struct {
	Title  string `json:"title"`
	Detail string `json:"detail"`
	Status int    `json:"status"`
}

// SetErrorResponse is a utility function to encode an error response with the given data.
func SetErrorResponse(w http.ResponseWriter, title string, detail string, status int) {
	response := &ErrorResponse{
		Title:  title,
		Detail: detail,
		Status: status,
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(response)
}
