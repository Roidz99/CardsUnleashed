// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"encoding/json"
	"net/http"

	"dsk/config"

	"github.com/rs/xid"
)

type joinGameRoomResponse struct {
	IPAddress string `json:"ip_address"`
	Port      int    `json:"port"`
}

// JoinGameRoomHandler is the handler of the "join_game_room" route.
func JoinGameRoomHandler(env *config.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Make sure the request comes from an authorized player.
		if !IsAuthorizedRequest(w, r, env) {
			return
		}

		// Make sure the request is a POST.
		if r.Method != http.MethodPost {
			SetErrorResponse(w, "Invalid method.", "This endpoint expects a POST method.", 400)
			return
		}

		// Parse the form parameters.
		r.ParseForm()
		param := r.FormValue("room_id")
		roomID, err := xid.FromString(param)
		if err != nil {
			SetErrorResponse(w, "Invalid room id.", "The given room id is not valid.", 400)
			return
		}

		room := env.GameRooms[roomID]
		if room == nil {
			SetErrorResponse(w, "Room does not exist.", "This room does not exist anymore.", 400)
			return
		}

		if room.NumPlayers == room.MaxPlayers {
			SetErrorResponse(w, "Room is full.", "This room is full.", 400)
			return
		}

		response := &joinGameRoomResponse{
			IPAddress: env.Config.IPAddress,
			Port:      room.Port,
		}

		if room.NumPlayers + 1 == room.MaxPlayers {
			room.GameStarted = true
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(response)
	})
}
