// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"net/http"
	"strconv"

	"dsk/config"

	"github.com/rs/xid"
)

// UpdateGameRoomHandler is the handler of the "update_game_room" route.
func UpdateGameRoomHandler(env *config.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Make sure the request is a POST.
		if r.Method != http.MethodPost {
			SetErrorResponse(w, "Invalid method.", "This endpoint expects a POST method.", 400)
			return
		}

		// Parse the form parameters.
		r.ParseForm()

		id, err := xid.FromString(r.FormValue("id"))
		if err != nil {
			SetErrorResponse(w, "Wrong parameter.", "This endpoint expects a id (string).", 400)
			return
		}

		internalID, err := xid.FromString(r.FormValue("internal_id"))
		if err != nil {
			SetErrorResponse(w, "Wrong parameter.", "This endpoint expects a internal_id (string).", 400)
			return
		}

		numPlayers, err := strconv.ParseInt(r.FormValue("num_players"), 10, 0)
		if err != nil {
			SetErrorResponse(w, "Wrong parameter.", "This endpoint expects a num_players (int).", 400)
			return
		}

		room := env.GameRooms[id]
		if room.InternalID == internalID {
			room.NumPlayers = int(numPlayers)
		}

		w.WriteHeader(200)
	})
}
