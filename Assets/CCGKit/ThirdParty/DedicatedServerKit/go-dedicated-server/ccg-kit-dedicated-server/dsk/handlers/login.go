// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"dsk/auth"
	"dsk/config"
	"dsk/models"

	"github.com/dgrijalva/jwt-go"
)

type loginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (request *loginRequest) Validate() bool {
	if strings.TrimSpace(request.Username) == "" {
		return false
	}

	if strings.TrimSpace(request.Password) == "" {
		return false
	}

	return true
}

type loginResponseSchema struct {
	Token string `json:"token"`
}

// LoginHandler is the handler of the "login" route.
func LoginHandler(env *config.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Make sure the request is a POST.
		if r.Method != http.MethodPost {
			SetErrorResponse(w, "Invalid method.", "This endpoint expects a POST method.", 400)
			return
		}

		// Parse the form parameters.
		r.ParseForm()
		username := r.FormValue("username")
		password := r.FormValue("password")
		request := &loginRequest{
			Username: username,
			Password: password,
		}

		// Validate the form parameters.
		if !request.Validate() {
			SetErrorResponse(w, "Missing parameters.", "This endpoint expects an username (string) and password (string).", 400)
			return
		}

		exists := models.UserWithNameExists(env.DB, username)
		if !exists {
			SetErrorResponse(w, "User does not exist.", "There is no user with that username.", 400)
			return
		}

		users, err := models.GetUsersByName(env.DB, username)
		if err != nil {
			SetErrorResponse(w, "Internal server error.", "A user could not be retrieved from the database.", 500)
			return
		}

		user := users[0]
		hashedPassword := user.Password
		match, err := auth.ComparePasswordAndHash(password, hashedPassword)
		if err != nil {
			SetErrorResponse(w, "Internal server error.", "There was an error while checking the user credentials.", 500)
			return
		}

		if !match {
			SetErrorResponse(w, "Incorrect password.", "The password is incorrect.", 401)
			return
		}

		expirationTime := time.Now().Add(5 * time.Minute)
		claims := &Claims{
			Username: username,
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: expirationTime.Unix(),
			},
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenString, err := token.SignedString([]byte(env.Config.JwtKey))
		if err != nil {
			SetErrorResponse(w, "Internal server error.", "A valid authorization token could not be generated.", 500)
			return
		}

		response := &loginResponseSchema{
			Token: tokenString,
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(response)
	})
}
