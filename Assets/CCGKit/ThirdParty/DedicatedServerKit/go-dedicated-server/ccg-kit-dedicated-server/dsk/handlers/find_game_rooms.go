// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package handlers

import (
	"encoding/json"
	"net/http"

	"dsk/config"
)

type responseGameRoom struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	MaxPlayers int    `json:"max_players,int"`
	NumPlayers int    `json:"num_players,int"`
}

type findGameRoomsResponse struct {
	GameRooms []responseGameRoom `json:"game_rooms"`
}

// FindGameRoomsHandler is the handler of the "find_game_rooms" route.
func FindGameRoomsHandler(env *config.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Make sure the request comes from an authorized player.
		if !IsAuthorizedRequest(w, r, env) {
			return
		}

		// Make sure the request is a POST.
		if r.Method != http.MethodPost {
			SetErrorResponse(w, "Invalid method.", "This endpoint expects a POST method.", 400)
			return
		}

		rooms := make([]responseGameRoom, len(env.GameRooms))
		i := 0
		for _, value := range env.GameRooms {
			if value.GameStarted != true {
				room := responseGameRoom{
					ID:         value.ID.String(),
					Name:	    value.Name,
					MaxPlayers: value.MaxPlayers,
					NumPlayers: value.NumPlayers,
				}
				rooms[i] = room
				i++
			}
		}

		rooms = rooms[:i]

		response := &findGameRoomsResponse{
			GameRooms: rooms,
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(response)
	})
}
