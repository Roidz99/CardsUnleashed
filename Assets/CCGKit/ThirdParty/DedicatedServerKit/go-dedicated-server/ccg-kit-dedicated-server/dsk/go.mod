module dsk

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/gddo v0.0.0-20200611223618-a4829ef13274
	github.com/rs/xid v1.2.1
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
)
