// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package config

import (
	"database/sql"
	"log"

	"dsk/gamerooms"

	"github.com/rs/xid"
)

// Env is used to pass contextual information to the HTTP request handlers.
type Env struct {
	Config      Configuration
	DB          *sql.DB
	GameRooms   map[xid.ID]*gamerooms.GameRoom
	CurrentPort int
	InfoLog     *log.Logger
	ErrorLog    *log.Logger
}
