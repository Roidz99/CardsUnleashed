// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package config

// Configuration stores the server-wide configuration information.
type Configuration struct {
	IPAddress          string `json:"ip_address"`
	Port               int    `json:"port,int"`
	DBConnectionString string `json:"db_connection_string"`
	GameServerBinPath  string `json:"game_server_bin_path"`
	GameServerPort     int    `json:"game_server_port,int"`
	JwtKey             string `json:"jwt_key"`
}
