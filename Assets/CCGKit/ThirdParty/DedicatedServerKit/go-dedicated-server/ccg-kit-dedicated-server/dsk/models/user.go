// Copyright (C) 2020 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

package models

import (
	"database/sql"
	"log"
)

// User represents a user of the system.
type User struct {
	ID       int
	Username string
	Email    string
	Password string
	Token    string
}

// UserWithEmailExists returns whether a user with the given email exists.
func UserWithEmailExists(db *sql.DB, email string) bool {
	var exists bool
	err := db.QueryRow("SELECT exists (SELECT * FROM users WHERE email = ?)", email).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false
	}
	return exists
}

// UserWithNameExists returns whether a user with the given name exists.
func UserWithNameExists(db *sql.DB, username string) bool {
	var exists bool
	err := db.QueryRow("SELECT exists (SELECT * FROM users WHERE username = ?)", username).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false
	}
	return exists
}

// GetUsersByName returns the users with the given name (will always be 1, as usernames are unique).
func GetUsersByName(db *sql.DB, username string) ([]*User, error) {
	rows, err := db.Query("SELECT * FROM users WHERE username = ?", username)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*User, 0)
	for rows.Next() {
		user := new(User)
		err := rows.Scan(&user.ID, &user.Username, &user.Email, &user.Password)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return users, nil
}

// AddUser stores the user with the given data into the system's database.
func AddUser(db *sql.DB, username string, email string, password string) error {
	stmt, err := db.Prepare("INSERT INTO users(username, email, password) VALUES(?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	res, err := stmt.Exec(username, email, password)
	if err != nil {
		log.Fatal(err)
	}
	_, err = res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	_, err = res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
