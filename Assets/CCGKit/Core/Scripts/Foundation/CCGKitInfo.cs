// Copyright (C) 2016-2022 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

namespace CCGKit
{
    /// <summary>
    /// CCG Kit version and copyright information.
    /// </summary>
    public static class CCGKitInfo
    {
        public static readonly string version = "2.0.0";
        public static readonly string copyright = "Copyright (C) 2016-2022 gamevanilla. All rights reserved.";
    }
}
